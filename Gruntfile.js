'use strict';

module.exports = function(grunt) {

    //Load NPM tasks
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('static-dev');
    grunt.loadNpmTasks('grunt-css');

    grunt.registerMultiTask("startserver", function(){
        startserver();
    });

    // Project Configuration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                harmony: true,
                process: function(src, filepath) {
                  if (filepath.substr(filepath.length - 2) === 'js') {
                    return '// Source: ' + filepath + '\n' +
                      src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
                  } else {
                    return src;
                  }
                }
            },

            css: {
                src: ['src/css/*.css'],
                dest: 'pages/css/aijiayuan.css'
            },

            chat:{
                src: ["src/js/common.js","src/js/tmpls.js","src/js/sync.js","src/js/chat.js"],
                dest: 'pages/js/chat.min.js'
            },

            index:{
                src: ["src/js/common.js","src/js/lovechain.js"],
                dest: 'pages/js/index.min.js'
            },

            lib:{
                src: ["src/js/lib/*.js"],
                dest: 'pages/js/lib.min.js'
            }
        },

        startserver:{
            xx:{src:"",dest:""}
        },

        uglify:{

            options: {
                banner: '/*! <%= pkg.name %> <%= pkg.version %> */\n',
                mangle: false
            },

            index: {
                src:  'pages/js/index.min.js',
                dest: 'pages/js/index.min.js'
            },

            chat: {
                src:  'pages/js/chat.min.js',
                dest: 'pages/js/chat.min.js'
            },

            lib: {
                src:  'pages/js/lib.min.js',
                dest: 'pages/js/lib.min.js'
            }
        },

        staticdev:{

            aijiayuan:{
                src  : "src/html/*",
                dest : "pages/"
            },
            templates:{
                src  : "src/html/templates/*",
                dest : "pages/templates/"
            },

            chats:{
                src  : "src/html/templates/chat/*",
                dest : "pages/templates/chat/"
            },
            indexs:{
                src  : "src/html/templates/index/*",
                dest : "pages/templates/index/"
            },
            search:{
                src  : "src/html/templates/chat/search/*",
                dest : "pages/templates/chat/search/"
            },
            feed:{
                src  : "src/html/templates/chat/feed/*",
                dest : "pages/templates/chat/feed/"
            }

        },

        cssmin:{
            xxx: {
                src:  ['pages/css/aijiayuan.css'],
                dest: 'pages/css/aijiayuan.min.css'
            },
            font: {
                src:  ['pages/css/awesome.css'],
                dest: 'pages/css/awesome.css'
            }
        },

        less: {
            zzz: {
                src:  ['src/css/*.less'],
                dest: 'src/css/less.css'
            }
        },

        watch: {
            css: {
                files: ['src/css/*'],
                tasks: ['less:zzz','concat:css', 'cssmin:xxx']
            },
            js: {
                files: ['src/js/*.js'],
                tasks: ["concat:index", "concat:chat",'uglify:index','uglify:chat']
            },
            html: {
                files: ["src/html/*","src/html/templates/*","src/html/templates/chat/*"],
                tasks: ['staticdev:aijiayuan',"staticdev:templates","staticdev:chats"]
            },
            indexs: {
                files: ["src/html/templates/index/*"],
                tasks: ["staticdev:indexs"]
            },
            search: {
                files: ["src/html/templates/chat/search/*"],
                tasks: ["staticdev:search"]
            }
        }
   
    });

    //Making grunt default to force in order not to break the project.
    grunt.option('force', true);

    //Default task(s).
    grunt.registerTask('default', ['startserver','staticdev','concat', 'cssmin','uglify','watch']);

    //Compile task (concat + minify)
    grunt.registerTask('compile', ['concat', 'cssmin', 'uglify']);

};
  


function startserver(){

    var koa = require('koa');
    var app = koa();
    var staticserver = require('koa-static');
    var config = require("./server/config.json");


    app.use(function *(next){

        yield next;

        if(this.path.indexOf(".") < 0){
            this.type = 'text/html';
            
            if(this.path.indexOf("/chat") == 0){
                this.response.body = readfile("chat");
            }else{
                this.response.body = readfile("index");
            }
        }

    });
    app.use(staticserver(__dirname+'/pages/'));
    app.use(function *(next){

        var file = {};
        for(var key in config){
            if( match(this.path, key) ){
                file = require("./server/"+config[key]);
                break;
            }
        }
        this.response.body = file;

    });

    function match(url, key){
        var key = key.replace(/\//g,"\\\/").replace(/{[a-zA-Z0-9]+}/g, "[a-zA-Z0-9]+");
        var reg = new RegExp(key);
        if(reg.test(url)) return key;
    }

    function readfile(name){
        return require("fs").readFileSync(__dirname+"/pages/"+name+".html").toString();
    }

    app.listen(8888);

}

