







var Router = require('koa-router'),
	k      = require("kmodel");

var restful = new Router();



restful.get("/:name/:id", function *(){

	var instance = k.load(this.params.name);

	

	if(instance){
		this.body = yield instance.find({_id: this.params.id});
	}

});


restful.get("/:name/:key/:value", function *(){

	var instance = k.load(this.params.name);
	var option = {};

	option[this.params.key] = this.params.value;

	if(instance){
		this.body = yield instance.find(option);
	}

});





restful.post("/:name/", function *(){

	var instance = k.load(this.params.name),
		object   = this.request.body,
		isexists, rst;

	if(this.session[object.phoneNumber]){
		if(Date.now() - this.session[object.phoneNumber] < 1000*60){
			this.body = { status: 1, msg: '60秒内不能重复注册'};
		}
	}

	var crypto = require('crypto');
    var shasum = crypto.createHash('sha1'),
    	code = String("0000"+Math.floor(Math.random()*10000)).slice(-4);;

    shasum.update(object.password);

    object.password = String(shasum.digest('hex'));

	if(instance){	

		if(object.phoneNumber){
			isexists = yield instance.find({phoneNumber: object.phoneNumber});
		}

		if(isexists && isexists.length > 0){
			this.body = { status: 1, msg: '该手机已经被注册'};
		}else{
			rst = yield instance.insertOne(object);
			rst[0].password = code
			// rst[0].password = "";
			this.session.user = rst[0];
			this.session[object.phoneNumber] = Date.now();
			SMS(code ,object.phoneNumber,{});

			this.body = rst[0];

		}
	}else{
		this.body = { status: 1, msg: '请求错误'};
	}

});

restful.post("/:name/:id", function *(){

	var instance = k.load(this.params.name),
		object   = this.request.body,
		isexists, rst;


	if(instance && this.session.user && this.session.user._id == this.params.id){	
		rst = yield instance.updateOne(object, {_id: this.params.id});
		this.body = rst;
	}else{
		this.body = { status: 1, msg: '请求错误'};
	}

});





module.exports = restful;











































































































































