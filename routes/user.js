

var Router = require('koa-router'),
	k      = require("kmodel");

var user = new Router();


var instance = k.load("User");


user.post("/signin", function *(next){

	var object   = this.request.body, rst;

	var crypto = require('crypto');
    var shasum = crypto.createHash('sha1');

    shasum.update(object.password);
    
    object.password = String(shasum.digest('hex'));

	rst = yield instance.queryOne(object);

	if(rst){
		this.session.user = rst;
		// this.cookies.set("userid", rst._id, {signed:true, });
		rst.password = "";
		this.body = rst;
	}else{
		this.body = { status: 1, msg: '账号或密码不存在'};
	}
});


user.get("/userinfo", function *(next){

	var user = yield instance.queryOne({_id:this.session.user._id});
	this.session.user = user;
	this.body = {status:0, msg: user};
});

user.get("/userinfo/:id", function *(next){

	var user =  yield instance.queryOne({_id:this.params.id});
	this.body =  user;
});



user.post("/update", function *(){

	var object = this.request.body;

	var rst = yield instance.updateOne(object, {_id: this.session.user._id});

	this.session.user = Array.isArray(rst) ? rst[0] : rst;
	this.body = rst;

});

// user.post("/updateuser", function *(){

// 	var object = this.request.body;

// 	var rst = yield instance.updateOne(object, {_id: this.user.session._id});

// 	this.body = rst ? {status:0, msg:"ok"} : { status: 1, msg: '账号或密码不存在'};

// });

var Image = k.load("Image"), fs = require("fs");

user.post("/upload", function *(){

	var object = this.request.body, rst;

	// var img = yield Image.queryOne({userid:this.session.user._id});

	// if(img){
	// 	rst = yield Image.updateOne(object, {userid: this.session.user._id});
	// }else{
	// 	object.userid = this.session.user._id;
	// 	rst = yield Image.insertOne(object);
	// }

	yield instance.updateOne({rotate: object.rotate}, {_id: this.session.user._id});

	fs.writeFileSync("pages/upload/"+this.session.user._id+".png", new Buffer(object.base64.slice(23), 'base64'))

	this.body = {};
});


user.post("/newimage", function *(){

	var object = this.request.body, rst;

	var crypto = require('crypto');
    var shasum = crypto.createHash('sha1');

    shasum.update(String(Date.now()));

    var str = shasum.digest('hex')+".png";


	fs.writeFileSync("pages/upload/"+str, new Buffer(object.base64.slice(23), 'base64'))

	this.body = str;
});


var Position = k.load("Position");

user.post("/position", function *(){

	var position = this.request.body, p, instance =  this.session.user;

	position = getBlock(position, 3);
	position.userid = instance._id;
	position.name = instance.name;
	position.gender = instance.gender;
	position.birthday = instance.birthday;
	position.signature = instance.signature || ""; 

	p = yield Position.queryOne({userid: instance._id});

	if(p){
		p = yield Position.updateOne(position, {userid: instance._id});
	}else{
		p = yield Position.insertOne(position);
	}

	this.body = p;
});


user.get("/image", function *(){

	var image = yield Image.queryOne({userid: this.session.user._id});
	
	this.type = "image/png";
	this.body = new Buffer(image.base64.slice(23), 'base64');
});

user.get("/position", function *(){

	var position = yield Position.queryOne({userid: this.session.user._id}), items, keys;

	items = yield Position.find({
								longitude : {$gt: position.longitudeMin, $lt: position.longitudeMax},
								latitude  : {$gt: position.latitudeMin,  $lt: position.latitudeMax} 
							});
	this.body = items;
});



var Feed = k.load("Feed");

user.post("/feed", function *(){


	var object = this.request.body;

	object.userid = this.session.user._id;
	object.name   = this.session.user.name;

	// var feeds = yield redis.get("feed"+this.session.user._id),
	var newfeed = yield Feed.insertOne(object);

	// if(feeds){
	// 	feeds = JSON.parse(feeds).concat([newfeed[0]]);
	// }else{
	// 	feeds = [newfeed[0]];
	// }
	// redis.set("feed"+this.session.user._id, JSON.stringify(feeds));

	this.body = newfeed;

});





var Friends = k.load("Friends");

user.get("/friends", function *(){

	var userids, data = {}, user, friends;

	if(this.session.user ){

		friends = yield Friends.queryOne({userid: this.session.user._id});
		userids = friends ? friends.list.split(",") : []

		var iter = function *(){
			for(var i =0; i < userids.length; i++) yield userids[i];
		};

		for(var id of iter()){
			user = yield instance.queryOne({_id: id});
			data[id] = user;
		}
	}

	this.body = data;

});

var Notifications = k.load("Notifications");

user.post("/addnotification", function *(){

	var body = this.request.body, n = {}, session = this.session.user;

	body.name = session.name;
	body.signature = session.signature;

	n.from    = session._id;
	n.message = body;
	n.to      = body.to;
	n.isread  = false;

	// console.log(getSocket(n.to))
	getSocket(n.to).emit("notifications");
	this.body = yield Notifications.insertOne(n);
});

user.get("/notifications", function *(){

	this.body = yield Notifications.find({to: this.session.user._id});
});

user.get("/checkphonenumber/:number", function *(){

	var number = this.params.number, rst;

	rst = yield instance.queryOne({phoneNumber: number});

	this.body = rst ? rst : {};

});

user.post("/accpet", function *(){

	var body = this.request.body, rst = {}, fs, id = this.session.user._id, fid = body.uid;


	fs = yield Friends.queryOne({userid: id});

	if(fs){
		if(fs.list && fs.list.indexOf(fid) < 0){
			fs.list += "," + fid;
		}
		fs = yield Friends.updateOne({list: fs.list}, {userid: id});
	}else{
		fs = yield Friends.insertOne({userid: id, list: fid});
	}

	id = fid;
	fid = this.session.user._id;

	fs = yield Friends.queryOne({userid: id});
	if(fs){
		if(fs.list && fs.list.indexOf(fid) < 0){
			fs.list += "," + fid;
		}
		fs = yield Friends.updateOne({list: fs.list}, {userid: id});
	}else{
		fs = yield Friends.insertOne({userid: id, list: fid});
	}

	var nid = body.nid;

	var update = yield Notifications.updateOne({state: "accept", isread: true}, {_id: body.nid});

	getSocket(body.uid).emit("notifications");

	this.body = rst;

	// getSocket(body.this.session.user._id).emit("notifications");
});


user.post("/refuse", function *(){

	var body = this.request.body, rst = {};
	var update = yield Notifications.updateOne({state: "refuse", isread: true}, {_id: body.nid});

	this.body = update;
});



// 获取新鲜事
user.get("/feed", function *(){


	var fs = yield Friends.queryOne({userid: this.session.user._id});

	var feeds = [], redisfeeds, list = fs ? fs.list.split(",") : [];

	list.push(this.session.user._id);

	var iter = function *(){
		for(var i =0; i < list.length; i++) yield list[i];
	};
	
	for(var userid of iter()){
		redisfeeds = yield Feed.find({userid: userid});
		if(redisfeeds) feeds = feeds.concat(redisfeeds);
	}

	this.body = feeds;
});


user.get("/myfeed", function *(){


	this.body = yield Feed.find({userid: this.session.user._id});
});

user.get("/feed/:uid", function *(){


	this.body = yield Feed.find({userid: this.params.uid});
});

user.post("/search", function *(){

	var body = this.request.body;


	var options = {}, name, aname;


	for(var name in body){

		aname = name.slice(6);

		if(body[name] != "不限" && body[name] != "undefined") options[aname] = body[name];

	}


	this.body = yield instance.find(options);

	// this.body = yield Feed.find({userid: this.session.user._id});
});

user.get("/userid/:id", function *(){

	var id = this.params.id;

	this.body = yield instance.queryOne({_id: id});

});

var Messages = k.load("Messages");

user.get("/message/:key", function *(){

	var key = this.params.key, rst;

	rst = yield Messages.limit({key: key}, 10);

	this.body = rst.reverse();

});


var FeedBack = k.load("FeedBack");

user.post("/feedback", function *(){

	var data = this.request.body;

	data.userid = this.session.user._id;

	var s = yield FeedBack.insertOne(data);

	this.body = s;

});


user.get("/vcode/:code", function *(){

	var code = this.params.code;
	// console.log(this.session.user)
	this.body = this.session.user.password == code ? {ok: true} : {ok:false};

});









module.exports = user;


function getBlock(position, zone){

	position.longitudeMax = (Math.floor(position.longitude*10) + zone)/10;
	position.longitudeMin = (Math.floor(position.longitude*10) - zone)/10;

	position.latitudeMax  = (Math.ceil(position.latitude*10) + zone)/10;
	position.latitudeMin  = (Math.ceil(position.latitude*10) - zone)/10;

	return position;
}



















