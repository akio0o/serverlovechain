


var redis = require("redis"),
	k     = require("kmodel"),
	thunkify = require("thunkify");


var userdao = k.load("User"), feeddao = k.load("Feed");



client = redis.createClient(6379, '127.0.0.1');

client.on("error", function(e){
	console.log(e)
});

var users = [], userid ;

var syncuser = function *(){

	users = yield userdao.find({}), items = [];
	
    users.map(function(user){
		client.set(user._id, JSON.stringify(user));
        userid = user._id;
	});

    var i = users.length, feeds;
    var iter = function *(){
        while(i-->0) yield users[i]._id;
    };


    for(var id of iter()){

        feeds = yield feeddao.find({"userid": id});

        client.set("feed"+id, JSON.stringify(feeds.sort(function(a, b){
            return a.ctime > b.ctime;
        })));
    }

};


client.get = function(){
    return thunkify(client.get);
}();
client.mget = function(){
    return thunkify(client.mget);
}();

// run(syncuser);

module.exports = client;




















function run (fn) {

    var gen = fn();

    next();

    function next(er, value){ 
        if (er) return gen.throw(er);
        var continuable = gen.next(value);

        if (continuable.done) return;
        var callback = continuable.value;
        callback(next);
    }
}

