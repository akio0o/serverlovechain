
exports.Image = function(k, opts){

    var opts = opts || {},
        isSalt = !!opts.isSalt;

    var Image = k.create({

        userid: 'string',
        base64: 'string'

    }, "Image");

    
    return Image;
};

