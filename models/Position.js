







exports.Position = function(k, opts){

    var opts = opts || {},
        isSalt = !!opts.isSalt;

    var Position = k.create({

        userid       : 'string',
        latitudeMax  : 'number',
        latitudeMin  : 'number',
        longitudeMax : 'number',
        longitudeMin : 'number',

        longitude    : 'number',
        latitude     : 'number',

        name         : 'string',
        gender       : 'string',
        signature    : 'string',
        birthday     : 'string'  

    }, "Position");

    
    return Position;
};

