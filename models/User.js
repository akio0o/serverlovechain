
exports.User = function(k, opts){

    var opts = opts || {},
        isSalt = !!opts.isSalt;

    var User = k.create({

        phoneNumber: "string", // 手机号码
        password: "string", // 密码
        name: "string", //真实名字
        birthday: "string",
        state: "string", // loving, single, married


        nickname: "string", //昵称
        signature: "string",
        gender: "string",
        height: "string", //身高

        // --- workinfo
        title:"string",
        company: "string",
        salary: "string",

        oldaddress : "string",
        address: "string",

        degree  : "string",
        college : "string",


        interesting : "string",
        religion: "string",

        house: "string",
        car  : "string",

        married: "string", // 是否结婚


        // wanted  择偶条件
        wanted: "object",

        rotate: "string",

        backgroundimg: "string"

    }, "User");

    
    return User;
};

