
exports.Feed = function(k, opts){

    var opts = opts || {},
        isSalt = !!opts.isSalt;

    var Feed = k.create({

        userid: 'string',
        
        imagelist: 'string',

        text: 'string',

        name: 'string'


    }, "Feed");

    
    return Feed;
};

