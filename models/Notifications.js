
exports.Notifications = function(k, opts){

    var opts = opts || {},
        isSalt = !!opts.isSalt;

    var Notifications = k.create({

        message: "object",
        isread : "boolean",
        from   : "string", 
        to     : "string",
        state  : "string"
        
    }, "Notifications");

    
    return Notifications;
};

