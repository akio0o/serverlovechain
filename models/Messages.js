
exports.Messages = function(k, opts){

    var opts = opts || {},
        isSalt = !!opts.isSalt;

    var Messages = k.create({

        key: "string",
        message: "string",
        from:"string",
        date: "string",
        to: "string",
        isread: "boolean",
        ctime: "date"
    }, "Messages");

    
    return Messages;
};

