





var koa = require("koa"),
	router = require("koa-router"),
	mount  = require("koa-mount"),
	kmodel = require("kmodel"),
	koaBody = require("koa-body"), xss = require("xss"),
    csrf = require('koa-csrf');
var gzip = require('koa-gzip');

var staticserver = require('koa-static');

var session = require('koa-session');

kmodel.connect("mongodb://vpn.uuzcloud.com:27017", __dirname+"/models/");


var app = koa();

var hash = "000000000000000002e445872f25e66eabb9b209e3d35858e197f99cfbba2415";

var router = require("./routes/lovechain");
var user = require("./routes/user");

app.keys = [ hash, 'author aki'];


app.use(session({secret: hash}));
app.use(gzip());
csrf(app);
app.use(csrf.middleware);
app.use(function *(next){

    yield next;

    if(this.path.indexOf(".") < 0 && this.method == "GET" && this.path.indexOf("/user") !=0 ){
        
        this.type = 'text/html';

        if(this.path.indexOf("/chat") == 0 && this.session.user){
            this.response.body = readfile("chat").replace("#crsftoken#", this.csrf);
        }else{
            this.response.body = readfile("index").replace("#crsftoken#", this.csrf);
        }
    }
});
app.use(staticserver(__dirname+'/pages/'));
app.use(koaBody({formLimit:10 * 1024 * 1024}));

app.use(function*(next){
    
    yield next;

    var postbody;

    if(this.method == "POST"){
        postbody = this.request.body;
        for(var key in postbody){
            if(postbody[key])
                this.request.body[key] = xss(postbody[key]);
        }
    }
});

app.use(mount("/restful", router.middleware()));
app.use(mount("/user", user.middleware()));


global.redis = require("./helper/userSync");
require("./helper/sms");
require("./sockets/webapp");

app.listen(5000);




function readfile(name){
    return require("fs").readFileSync(__dirname+"/pages/"+name+".html").toString();
}





