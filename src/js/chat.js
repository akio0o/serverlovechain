void function(){

	var body = $("body"), views = $(".views"), arrow = $(".nav-arrow"), user = S.get("user");

	body.show();

	body.on("touchmove", function(e){
		var target = $(e.target).parents(".content>.touchable");
		
		if(target.css("overflow-y")!="auto") e.preventDefault();
	});


	$(".swiper-slide,.wrapper").css("height",window.innerHeight/4).width(window.innerWidth)
	.css("position","relative");

	window.Swiper = new Swiper('.swiper-container',{
		speed: 300,
		noSwiping:true,
		noSwipingClass: "touchable"
	});

	body.on("tap", ".bottom-bar .item", function(){

		var me     = $(this), 
			column = me.attr("column"), 
			el     = $("."+column);

		S.setString("tabindex", column);

		me.addClass("current").siblings().removeClass("current");
		el.show().siblings(".navlist").hide();
	});


	var height = window.innerHeight;

	if(location.pathname == "/chat") height -= 95;

	views.css("height", height);

	UI.add("/chat", {
		template: "/templates/chat/index.html",
		ishide: true,
		title: "LoveChain",
		init: function(){
			
			var tabindex = S.getString("tabindex") || false;
			var dom = $(Swiper.activeSlide());

			if(tabindex) dom.find("."+tabindex+"btn").trigger("tap");

			dom.find(".mine .name").html(S.get("user").name);
			if(MC.fs) dom.find(".notifications").html(MC.fs).show();

			dom.find(".mycontacts").html(T.friends({list: MC.list}));
			dom.find(".chatlist").html(T.chatlist({contacts: MC.messagelist}));

			// dom.find(".content").css("height",$(window).height()-90).css("overflow-y","auto")
			// 	.addClass("touchable")
			// 	.css("-webkit-transform","translate3d(0,0,0)");
		}
	})



	.add("/chat/message", {
		template: "/templates/chat/message.html",
		target: "/chat",
		callback: function(el, parent, callback){

			var title = el.find("h3").html(), current = el.attr("mid");

			S.set("messageid", current);
			MC.clear(current);
			console.log(el.find(".newmsg").html())

			el.find(".newmsg").html("");
			return true;
		},

		init: function(){


			var dom = $(Swiper.activeSlide()), key, 
				id =  S.get("user")._id ,
				messageid = S.get("messageid");
			
			var user = MC.list[messageid] || false;

			if(user == false){
				$.get("/user/userinfo/"+messageid, function(data){
					user = data;
					dom.find(".nav").html(user.name);
					MC.addmessage(user);
				});
			}else{
				dom.find(".nav").html(user.name);
				MC.addmessage(user);
			}


			//: translate3d(0,0,0)

			// var instance = {}, contactlist = S.get("contactlist");

			// if(!contactlist) return;

			// contactlist.map(function(c){
			// 	if(c._id == messageid){
			// 		instance = c;
			// 		c.newmsg = 0;
			// 		return false;
			// 	}
			// });

			// S.set("contactlist", contactlist);
			// socket.emit("savemsg", contactlist);

			

			// socket.emit("history", {from: S.get("user")._id, to: messageid});

			dom.on("keyup", ".inputtext", function(event){
				if(event.keyCode == 13 && this.value != ""){
					sendMessage(this.value);
					this.value = "";
				}
			}).on("focusout", ".inputtext", function(){
				// if(this.value.length > 0){
					// sendMessage(this.value);
				// }
			});

			key = messageid > id ? messageid+id : id+messageid;

			$.get("/user/message/"+key, function(data){

				var html = "";

				data.forEach(function(m){
					m.id =  m.from == id ? id : m.from;
					html += m.from == id ? T.minemessage(m) : T.message(m);
				});
				S.scrollable(dom, ".messages", 90);

				Sync.append(".messages", html);

			});


			function sendMessage(text){

				var message = {};
				message.message = text;
				message.to = messageid;
				message.from = S.get("user")._id;
				message.isread = false;

				socket.emit("message", message);

				message.id = message.from;

				Sync.append(".messages", T.minemessage(message));

			}

		}
	})






	.add("/chat/info", {

		template: "/templates/chat/info.html",
		title: "",
		nocache: true,
		target: "/chat",
		callback: function(el, parent, callback){
			var id = el.attr("uid");
			localStorage.userid = id;
			return true;
		},
		init: function(){
			var dom = $(Swiper.activeSlide());
			$.get("/user/userid/"+localStorage.userid, function(x){

				renderInfo(dom, x, false);

			});
		}
	})

	.add("/chat/searchinfo", {

		template: "/templates/chat/info.html",
		title: "",
		target: "/chat/search",
		callback: function(el, parent, callback){

			var id = el.attr("uid");
			localStorage.userid = id;
			return true;
		},
		init: function(){
			var dom = $(Swiper.activeSlide());
			$.get("/user/userinfo/"+localStorage.userid, function(x){
				

				renderInfo(dom, x, !MC.list[localStorage.userid]);

				dom.off().on("tap", ".requestfriend", function(event){
					var notification = {};
					notification.type = "addfriend";
					notification.number = x.phoneNumber;

					notification.to     = x._id;
					notification.from   = S.get("user")._id;
					console.log(notification)
					$.post("/user/addnotification", notification, function(){
						alert("请求已发送,等待接受");
					});
				});

			});

		}
	})
		
	.add("/chat/nearbyinfo", {

		template: "/templates/chat/info.html",
		title: "",
		target: "/chat/nearby",
		callback: function(el, parent, callback){

			var id = el.attr("uid");
			localStorage.userid = id;
			return true;
		},
		init: function(){
			var dom = $(Swiper.activeSlide());
			$.get("/user/userinfo/"+localStorage.userid, function(x){
				

				renderInfo(dom, x, !MC.list[localStorage.userid]);

				dom.off().on("tap", ".requestfriend", function(event){
					var notification = {};
					notification.type = "addfriend";
					notification.number = x.phoneNumber;

					notification.to     = x._id;
					notification.from   = S.get("user")._id;
					console.log(notification)
					$.post("/user/addnotification", notification, function(){
						alert("请求已发送,等待接受");
					});
				});

			});

		}
	})


	.add("/chat/search", {

		template: "/templates/chat/search.html",
		title: "搜索",
		target: "/chat",
		init: function(){
			var dom = $(Swiper.activeSlide());

			var items = ["searchage","searchheight","searchdegree","searchwork","searchsalary","searchaddress"];

			
			items.forEach(function(item){

				if(localStorage[item])
					dom.find("."+item+" .now").html(localStorage[item]);
			});

			dom.off().on("tap", ".searchbtn", function(){

				var data = {};
				items.forEach(function(item){
					data[item] = localStorage[item];
				});

				$.post("/user/search",data, function(data){
					var html = T.search({list: data});
					dom.find(".result").html(html);
					S.scrollable(dom, ".search");

				});	
			});

			dom.find(".searchbtn").trigger("tap");

		}
	})


	.add("/chat/comecross", {

		template: "/templates/chat/comecross.html",
		title: "遇见",
		nocache: true,
		target: "/chat"
	})



.add("/chat/addcontact", {

	template: "/templates/chat/addcontact.html",
	title: "添加好友",
	nocache: true,
	target: "/chat",
	init: function(){

		var dom = $(Swiper.activeSlide());
		var text = dom.find(".phonenumber"), n = localStorage.phonenumber;

		if(n) text.val(n);

		dom.off().on("tap", ".btn", function(){

			var notification = {};

			if(/^\d{11}$/.test(text.val())){
				
				text.css("border","1px solid #ccc");
				
				notification.type = "addfriend";
				notification.number = text.val();

				$.get("/user/checkphonenumber/"+text.val(), function(data){
					if(data._id){
						notification.to     = data._id;
						notification.from   = S.get("user")._id;
						$.post("/user/addnotification", notification, function(){
							alert("发送成功");
						});
					}else{
						alert("该用户还未注册");
					}
				});

			}else{
				text.css("border","1px solid red");
			}

		}).on("input", ".phonenumber", function(){
			localStorage.phonenumber = this.value;
		});

	}
})






	.add("/chat/nearby", {
		template: "/templates/chat/nearby.html",
		title: "附近的人",
		target: "/chat",

		callback: function(el, parent, callback){

			var locationSuccess = function(position){
				 var coords = {};
				 coords.latitude = position.coords.latitude;   
				 coords.longitude = position.coords.longitude;  

				 S.set("coords", coords);
				 
				 $.post("/user/position", coords, function(data){
				 	callback();
				 });
			};
			var locationError = function(e){
				console.log(e)
			};

			if (navigator.geolocation) {
			    navigator.geolocation.getCurrentPosition(locationSuccess, locationError,{
			        enableHighAcuracy: true,
			        timeout: 5000,
			        maximumAge: 5000
			    });
			}else{
			    return false;
			}
		},

		init: function(){

			var coords = S.get("coords"), dom = $(Swiper.activeSlide());

			$.get("/user/position", function(data){
				
				var html, items;

				items = data.map(function(d){
					d.distance = S.distance(coords, d);
					return d;
				});

				html = T.nearby({list:items});

				$(".nearby").html(html);

				S.scrollable(dom, ".nearby");

			});
		}
	})



	.add("/chat/hit", {

		template: "/templates/chat/hit.html",
		title: "搭讪",
		nocache: true,
		target: "/chat"
	})

	.add("/chat/myfeed", {

		template: "/templates/chat/myfeed.html",
		title: "我的动态",
		nocache: true,
		target: "/chat/feeds",
		init: function(){

			$.get("/user/myfeed", function(data){
				render(data, true)
			});
			
			var dom = $(Swiper.activeSlide()), bg = S.get("user").backgroundimg;

			if(bg) dom.find(".bg").css("background","url(/upload/"+bg+")");
		}
	})


	.add("/chat/smyfeed", {

		template: "/templates/chat/feed/others.html",
		title: "动态",
		target: "/chat/searchinfo",
		callback: function(el, parent, callback){

			var uid = el.attr("uid");

			S.setString("feeduserid", uid);

			return true;
		},

		init: function(){

			$.get("/user/feed/"+S.getString("feeduserid"), function(data){
				$.get("/user/userinfo/"+S.getString("feeduserid"), function(user){

					render(data, true, user);
				});
			});
			
			// var dom = $(Swiper.activeSlide()), bg = S.get("user").backgroundimg;

			// if(bg) dom.find(".bg").css("background","url(/upload/"+bg+")");
		}
	})

	.add("/chat/feed", {

		template: "/templates/chat/feed/others.html",
		title: "动态",
		target: "/chat/feeds",

		callback: function(el, parent, callback){

			var uid = el.attr("uid");

			S.setString("feeduserid", uid);

			return true;
		},

		init: function(){

			$.get("/user/feed/"+S.getString("feeduserid"), function(data){
				$.get("/user/userinfo/"+S.getString("feeduserid"), function(user){

					render(data, true, user);
				});
			});
			

			// if(bg) dom.find(".bg").css("background","url(/upload/"+bg+")");
		}
	})


	.add("/chat/feeds", {

		template: "/templates/chat/feeds.html",
		title: "动态",
		nocache: true,
		target: "/chat",
		init: function(){

			var dom = $(Swiper.activeSlide()), bg = S.get("user").backgroundimg;

			$.get("/user/feed", function(data){
				render(data);
			});

			if(bg) dom.find(".bg").css("background","url(/upload/"+bg+")");
			
		}
	})

	.add("/chat/newfeed", {

		template: "/templates/chat/newfeed.html",
		title: "",
		nocache: true,
		target: "/chat/feeds"
	})

	

	.add("/chat/me", {

		template: "/templates/chat/me.html",
		title: "我",
		target: "/chat",
		init: function(){

			var profile = $(".profile");
			var dom = $(Swiper.activeSlide());

			profile.css("height",$(window).height()-40).parent().css("-webkit-overflow-scrolling","touch");

			profile.find(".header img").attr("src", "/upload/"+S.get("user")._id+".png").hide();
			$.getJSON("/user/userinfo?_="+Date.now(), function(data){

				var key, el , value;

				if(data.status == 0){
					S.set("user", data.msg);
					for( key in data.msg){
						
						el    = dom.find("#"+key).find(".now");
						value = data.msg[key];

						if(value == "male") value = "男";
						if(value == "female") value = "女";
						el.html( value|| "");
					}
					if(data.msg.rotate) profile.find(".header img").css("-webkit-transform", "rotate("+data.msg.rotate+"deg)");
					profile.find(".header img").show();
				}
			});

		}
	})

	

	.add("/chat/feedback", {

		template: "/templates/chat/feedback.html",
		title: "反馈",
		nocache: true,
		target: "/chat",

		init: function(){
			var dom = $(Swiper.activeSlide());

			dom.off().on("tap",".btn", function(){

				var value = dom.find("textarea").val();

				if(value == "") return alert("说点什么吧");

				$.post("/user/feedback", {text: value}, function(){
					alert("发送成功");
				});
			});

		}
	})



	.add("/chat/newfriend", {

		template: "/templates/chat/newfriend.html",
		title: "好友请求",
		target: "/chat",
		callback: function(el, parent, callback){
			el.find(".notifications").hide();
			return true;
		},
		init: function(){

			var dom = $(Swiper.activeSlide());

			dom.find(".newfriend").html(T.notifications({list:MC.notifications}))
				.css("overflow-y","auto").css("height",$(window).height()-40)
				.css("-webkit-overflow-scrolling","touch");
			
			dom.off().on("tap", ".add", request).on("tap", ".refuse", request);
		}
	})

	.add("/chat/texteditor", {

		template: "/templates/chat/texteditor.html",
		title: "编辑",
		nocache: true,
		target: "/chat/me",

		callback: selectsomething
	})

	.add("/chat/addressselect", {

		template: "/templates/chat/addressselect.html",
		title: "地址",
		nocache: true,
		target: "/chat/me",

		callback: selectsomething

	})

	.add("/chat/editsalary", {

		template: "/templates/chat/editsalary.html",
		title: "薪水",
		nocache: true,
		target: "/chat/me",

		callback: selectsomething
	})

	.add("/chat/degreeselect", {

		template: "/templates/chat/degree.html",
		title: "学历",
		nocache: true,
		target: "/chat/me",

		callback: selectsomething
	})

	.add("/chat/college", {

		template: "/templates/chat/college.html",
		title: "学校",
		nocache: true,
		target: "/chat/me",

		callback: selectsomething
	})

	.add("/chat/ismarried", {

		template: "/templates/chat/ismarried.html",
		title: "婚姻状况",
		nocache: true,
		target: "/chat/me",

		callback: selectsomething
	})

	.add("/chat/search/age", {

		template: "/templates/chat/search/age.html",
		title: "选择年龄段",
		target: "/chat/search",
		callback: searchselectsomething,
		init: function(){
			selectsearch(["不限","18-21","22-24","25-28","29-40","41-"]);
		}
	})

	.add("/chat/search/height", {

		template: "/templates/chat/search/height.html",
		title: "选择身高",
		target: "/chat/search",
		callback: searchselectsomething,
		init: function(){
			selectsearch(["不限","160+","165+","170+","180+","185+"]);
		}
	})
	


	.add("/chat/search/level", {

		template: "/templates/chat/search/level.html",
		title: "选择文化水平",
		target: "/chat/search",
		callback: searchselectsomething,
		init: function(){
			selectsearch(["不限","小学","高中","大学","硕士","博士"]);
		}
	})


	.add("/chat/search/salary", {

		template: "/templates/chat/search/salary.html",
		title: "薪资范围",
		target: "/chat/search",
		callback: searchselectsomething,
		init: function(){
			selectsearch(["不限","0-5000","5000-10000","10000-20000","20000-50000","50000+"]);
		}
	})

	.add("/chat/search/work", {

		template: "/templates/chat/search/work.html",
		title: "选择工作信息",
		target: "/chat/search",
		callback: searchselectsomething,
		init: function(){
			selectsearch(["不限","小学","高中","大学","硕士","博士"]);
		}
	})

	.add("/chat/search/address", {

		template: "/templates/chat/search/address.html",
		title: "选择住址",
		target: "/chat/search",
		callback: searchselectsomething
		// init: function(){
		// 	selectsearch(["小学","高中","大学","硕士","博士"]);
		// }
	})












	;


	// S.getString("currentpage").indexOf("/chat") == 0 ? UI.render(S.getString("currentpage")) : UI.render();

	window.onload = function(){
		// console.log(window.innerHeight, window.innerWidth)
		// var scroll = new iScroll("views");
	};



	function selectsomething(el, parent, callback){
		var type = el.attr("id");
		localStorage.editkey = type;
		return true;
	}

	function searchselectsomething(el, parent, callback){
		var type = el.attr("id");
		localStorage.editsearchkey = type;
		return true;
	}



 // 渲染feed

	function render(data, myfeed, user){
				
		var parent = $(Swiper.activeSlide()), html = "", user = user || S.get("user");

		var feeds = parent.find(".feeds"), dom = feeds.parent().parent().find(".nav");

		feeds.css("max-height",$(window).height()-40).css("overflow-y","auto").addClass("touchable")
			.parent().css("-webkit-overflow-scrolling","touch");
		
		feeds.find(".myheader").attr("src", "/upload/"+user._id+".png");

		feeds.find(".name").html(user.name);

		dom.off().on("change", ".feedfile", function(){
			S.readerfile(this.files[0], S.feed);
		});


		if(myfeed !== true){
			dom.html('动态<i class="fa-camera fa"></i><input type="file" \
				class="feedfile touchable" accept="image/*; capture=camera" >');
		}

		var feed;

		while(data.length){
			feed = data.shift();
			feed.imgs = feed.imagelist.split(",");
			html += T.feed(feed);
		}

		// data.forEach(function(feed){

			
		// });

		$(html).insertAfter(feeds.find(".bg"));

		parent.off().find(".backgroundinput").on("change", function(){
			
			S.addFeedImage(this.files[0], function(filename){
				console.log(filename)
				$.post("/user/update", {backgroundimg: filename}, function(data){
					S.set("user", data);
					dom.find(".bg").css("background","url(/upload/"+data.backgroundimg+")");
				});
			});
		});

		parent.on("tap", ".imagelist img", function(e){

			var preview = $(".preview"), w = $(this).width(), h = $(this).height();

			preview.find("img").attr("src", this.src).css({
				width: window.width,
				height: window.width*h/w,
				left: 0,
				"margin-top": -window.width*h/w/2,
				right:0,
				top: "50%"
			});
			preview.show();

		});
	}





	function selectsearch(array){

		var user = S.get("user"), type = localStorage.editkey,
			data = {}, leftarrow = $(".leftarrow");

		var parent = $(Swiper.activeSlide());

		var tmpl = 
		'{{#each list}}<div class="item touchable" >\
			<span>{{this}}</span>\
		</div>{{/each}}';


		var templatedata = {list: array};

		var template = Handlebars.compile(tmpl);

		var string = template(templatedata);


		var select = parent.find(".navlist .items");

		var user = S.get("user"), editkey = localStorage.editkey, postdata = {};

		select.html(string);
		select.css("height",$(window).height()-40).parent()
		.css("overflow-y","auto").css("-webkit-overflow-scrolling","touch");


		select.off().on("tap", ".item", function(){

			var me = $(this), key = me.find("span").html();

			// postdata[editkey] = key;
			// $.post("/restful/User/"+user._id, postdata, function(data){
			// });
			localStorage[localStorage.editsearchkey] = key;
			leftarrow.trigger("tap");
		});
	}

	function request(){

		var data = {}, me = $(this),
			uid = this.getAttribute("uid"), nid = this.getAttribute("nid");

		data.uid = uid;
		data.nid = nid;

		var url = me.hasClass("add") ? "/user/accpet" : "/user/refuse";
		var classname = me.hasClass("add") ? "add" : "refuse";
		var text = me.hasClass("add") ? "已接受" : "已拒绝";

		$.post(url, data, function(data){
			if(data){
				MC.sync();
				me.removeClass("btn").removeClass(classname).html(text).siblings(".btn").remove();
			}
		});
	}

	function renderInfo(dom, x, ismyfriend){
		var date, times, html, age;

		date  = new Date(x.birthday);
		times = Date.now() - new Date("1992-09-17");
		x.age   = Math.floor(times/(365*24*60*60*1000));

		x.ismyfriend = ismyfriend;
		x.list = [];
		console.log(x)

		$.get("/user/feed/"+localStorage.userid, function(data){

			data.forEach(function(y){
				x.list = x.list.concat(y.imagelist.split(","));
			});
			console.log(x)
			x.list = x.list.slice(0,3);

			html  = T.info(x);
			dom.find(".info").html(html);

		});
	}




	setTimeout(S.download, 1000);




}();
// .add("/chat/services", {

	// 	template: "/templates/chat/services.html",
	// 	title: "服务",
	// 	nocache: true,
	// 	target: "/chat"
	// })

	// .add("/chat/shopping", {

	// 	template: "/templates/chat/shopping.html",
	// 	title: "购物",
	// 	nocache: true,
	// 	target: "/chat"
	// })

	// .add("/chat/activity", {

	// 	template: "/templates/chat/activity.html",
	// 	title: "活动",
	// 	nocache: true,
	// 	target: "/chat"
	// })

	// .add("/chat/iday", {

	// 	template: "/templates/chat/iday.html",
	// 	title: "重要日子",
	// 	nocache: true,
	// 	target: "/chat"
	// })


	// .add("/chat/dating", {

	// 	template: "/templates/chat/dating.html",
	// 	title: "约会",
	// 	nocache: true,
	// 	target: "/chat"
	// })
// .add("/chat/like", {

	// 	template: "/templates/chat/like.html",
	// 	title: "收藏",
	// 	nocache: true,
	// 	target: "/chat"
	// })

	// .add("/chat/wallet", {

	// 	template: "/templates/chat/wallet.html",
	// 	title: "钱包",
	// 	nocache: true,
	// 	target: "/chat"
	// })

	// .add("/chat/address", {

	// 	template: "/templates/chat/address.html",
	// 	title: "收货地址",
	// 	nocache: true,
	// 	target: "/chat"
	// })

	// .add("/chat/emotion", {

	// 	template: "/templates/chat/emotion.html",
	// 	title: "感情状态",
	// 	nocache: true,
	// 	target: "/chat"
	// })