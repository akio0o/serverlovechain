



void function(){

	// 存储 url 配置
	var configs = {};

	// 记录页面是否已经加载
	var cache = {}, version = "1";

	var arrow = $(".nav-arrow"), body = $("body");

	var getTemplate = function(url, config){


		var dom = $(Swiper.activeSlide());
		dom.find(".content").hide();

		if(config.title) dom.find(".nav").html(config.title);

		S.setString("currentpage", url);

		if(S.get(config.template)){
			dom.find(".content").html(S.get(config.template)).show();
			config.init && config.init();
			return;
		}

		$.ajax({type: "GET", url : config.template}).done(function(data){

			dom.find(".content").html(data).show();

            var store = {};
            store.html = data;
            store.time = Date.now();
            S.set(url , store);
            
			config.init && config.init();
		});
	};

	var UI = {

		// 如果未配置，则隐藏 

		arrow: function(config){
			
			arrow[config.ishide ? "hide" : "show"]();

			if(config.target){
				arrow.attr("action", config.target);
			}

			if(typeof config.title == "string") $(Swiper.activeSlide()).find(".nav").html(config.title);
		},

		// 渲染

		render: function(action){

			var url = action|| location.pathname, config = configs[url];

			// 渲染页面

			getTemplate(url, config);

			if(url) history.pushState({}, "", url);
			// 初始化 arrow


			UI.arrow(config);

		},

		add: function(url, config){
			
			configs[url] = config;

			return this;
		},

		title: function(url, title){
			configs[url].title = title;
		}

	};


	var Animate = {

		nextPage: function(el, type){

			var action = el.attr("action"),
	    		type   = type || el.attr("action-type");

	    	// 做图形切换
	    	Swiper[type == "next" ? "swipeNext" : "swipePrev"]();

	    	// 切完之后改变URL值
	    	history.pushState({}, "", action);

	    	// 加载页面 -- 如果已经加载则无需加载

	    	if(action == "/chat" && cache[action]){
	    		UI.arrow(configs[action]);
	    	}else{
	    		UI.render();
	    	}


	    	// if(configs[action].nocache || !cache[action]){
	    		
	    	// }else{
	    		// UI.arrow(configs[action]);
	    	// }

		}

	};

	

	$("body").on("tap", ".swipe", function(){

		var me = $(this), action = me.attr("action");

    	if(configs[action].callback){
			configs[action].callback(me, $(Swiper.activeSlide()), function(){
				Animate.nextPage(me)
			}) && Animate.nextPage(me);
		}else{
			Animate.nextPage(me);
		}
    });


    $("body").on("tap", ".leftarrow", function(){

    	var dom = $(Swiper.activeSlide());

    	dom.find("input,textarea").blur();

    	Animate.nextPage($(this), "prev");
		
		S.setString("currentpage", $(this).attr("action"));

    });

    var common = {


    	isPhoneNumber: function(number){

    		return /^\d{11}$/.test(number);
    	},

    	isPassword: function(pass){
    		return pass.length > 6;
    	},

    	isVerifyCode: function(code){
    		return /^\d{4}$/.test(code);
    	},


    	isName: function(name){
    		return name.length > 1;
    	},

    	isBirthday: function(birthday){

    		return birthday.length > 2;
    	},

    	verify: function(func, el){
    		if(func(el.val()) == false){
    			el.css("border","1px solid red");
    			return false;
    		}
    		el.css("border","1px solid #ccc");
    		return true;
    	}

    };


    var S = {

    	set: function(name, value){

            var data = "";
            try{
                data = JSON.stringify(value);
            }catch(e){

            }
    		localStorage[name] = data;
    	},

    	get: function(name){
            var data = "";
            try{
                data = JSON.parse(localStorage[name]);
            }catch(e){

            }
    		return data;
    	},

    	setString: function(name, str){
    		localStorage[name] = str;
    	},

    	getString: function(name){
    		return localStorage[name];
    	}

    };


    body.on("tap", ".group span", function(){
    	$(this).removeClass("unchecked").siblings().addClass("unchecked");
    });


    S.rad = function(d){

    	return d * Math.PI / 180.0;
    };

    S.distance = function(pointA, pointB){
    	
    	var latA, latB, lat, lng;

    	pointA.latitude = parseFloat(pointA.latitude);
    	pointB.latitude = parseFloat(pointB.latitude);
    	pointA.longitude = parseFloat(pointA.longitude);
    	pointB.longitude = parseFloat(pointB.longitude);

		latA = S.rad(pointA.latitude),
		latB = S.rad(pointB.latitude),
		lat  = latA - latB,
		lng  = S.rad(pointA.longitude) - S.rad(pointB.longitude);

    	var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(lat/2),2) +
    			Math.cos(latA)*Math.cos(latB)*Math.pow(Math.sin(lng/2),2)));

    	s = s * 6378.137*1000;

    	s = Math.round(s * 10000) / 10000;

    	return s.toFixed(2);
    };



    S.readerfile = function(file, callback){

    	var reader  = new FileReader(), base64;

		reader.onloadend = function () {
			base64 = reader.result;

			callback(base64);
		}

		reader.readAsDataURL(file);
			
    };

    S.next = function(action){


    	Swiper.swipeNext();
    	history.pushState({}, "", action);
		UI.render();

    };

    S.feed = function(base64){


    	S.uploadfile(base64, function(filename){

    		var action = "/chat/newfeed", imagelist = S.get("imagelist") || [];
    		
    		imagelist.push(filename);

            S.set("imagelist", imagelist);

            S.next(action);

	    	$(Swiper.activeSlide()).find(".nav").html("");

    	});
    };


    S.download = function(){

        var url, templates =[], loading = $(".loading"), width, templatewidth = 0, i = 0, max;

        for( url in configs){
            templates.push(configs[url].template);
        }

        width = loading.find(".progress .perecnt").width();

        max   = templates.length;
        S.gethtml( templates.shift(), callback);


        function callback(done){

            i++;

            if(done == "done"){
                templatewidth = width;
                UI.render(); 
            }else{
                templatewidth = width*i/max;
            }
            loading.find(".progress .perecnt .value").css("width", templatewidth+"px");
            loading.find(".progress .cvalue").html((100*templatewidth/width).toFixed(1) + "%");

            if(done!="done") S.gethtml(templates.shift(), callback);
            else{
                $(".loading").hide();
            }
        }
    };

    S.gethtml = function(url, callback){

        if(url == undefined) return callback("done");

        if(S.get(url)){

            return callback();
        }

        $.ajax({type: "GET", url :  url}).done(function(data){
            S.set(url, data);
            callback();
        }).fail(function(){
            console.log("error")
            callback();
        });
    };

    


    S.addFeedImage = function(file, callback){

    	S.readerfile(file, function(base64){
    		S.uploadfile(base64, callback);
    	});
    };


    S.uploadfile = function(base64, callback){
    	
    	

    	var canvas = document.getElementById("tempcanvas"),
    		ctx = canvas.getContext("2d"),
    		img    = document.createElement("img");

    	var scale = 2;



    	img.onload = function(){

    		if(img.width > 1400) scale = 10;

    		canvas.height = img.height/scale;
    		canvas.width  = img.width/scale;

    		ctx.drawImage(img, 0,0,img.width/scale,img.height/scale);
    		var base64 = canvas.toDataURL('image/jpeg',0.7);

    		$.post("/user/newimage", {base64: base64}, function(filename){

    			callback(filename);

    		});

    	};
    	img.hidden = true;
    	img.src = base64;

    	document.body.appendChild(img);
    };

    S.clearcache = function(){

        for(var url in localStorage){
            if(url.indexOf("/templates/") == 0){
                localStorage[url] = null;
            }
        }

    };

    S.scrollable = function(dom, classname, height){

        var height = height || 40;

        dom.find(classname).css("height",$(window).height()-height).css("overflow-y","auto")
            .addClass("touchable").css("-webkit-overflow-scrolling","touch");
    };

    var preview = $(".preview");

    preview.on("tap", function(){
        preview.hide();
    });

    if(localStorage.download){
        var time = +Date.now() - (+localStorage.download);
        console.log(time)
        if(time > 12 * 60 * 60 *1000){
            S.clearcache();
            localStorage.download = Date.now();
        }

    }else{
        S.clearcache();
        localStorage.download = Date.now();
    }

    window.common = common;

   	window.S  = S;

    window.UI = UI;
    window.width = $(window).width();

    // window.Animate = Animate;
    $.ajaxSettings.beforeSend= function(xhr){
        xhr.setRequestHeader("X-Csrf-Token", $("#token").val());
    };



}();


