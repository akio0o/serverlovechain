




var T  = {

	chatlist: '\
	{{#each contacts}}\
		<div class="item touchable swipe {{_id}}" action="/chat/message" action-type="next" mid="{{_id}}">\
		<img src="/upload/{{_id}}.png" \
			{{#if rotate}} style="-webkit-transform:rotate({{rotate}}deg){{/if}}"\
		 class="touchable swipe" action="/chat/info" action-type="next" height="40" width="40">\
		<span class="newmsg">{{newmsg}}</span>
		<div class="box">\
			<h3>{{name}}</h3>\
			<p>{{message}}</p>\
		</div>\
		<div class="time">\
			\
		</div>\
	</div>\
	{{/each}}',

	message: '\
		<div class="message">\
			<div class="image">\
				<img src="/upload/{{id}}.png" height="40"\
				{{#if rotate}} style="-webkit-transform:rotate({{rotate}}deg){{/if}}"\
				 width="40">\
			</div>\
			<div class="messagebox touchable">\
				{{message}}\
			</div>\
		</div>',

	minemessage: '\
		<div class="message">\
			<div class="image right">\
				<img src="/upload/{{id}}.png"\
				{{#if rotate}} style="-webkit-transform:rotate({{rotate}}deg){{/if}}"\
				 height="40" width="40">\
			</div>\
			<div class="messagebox touchable right">\
				{{message}}\
			</div>\
		</div>',

	nearby: 
	'{{#each list}}<div class="item touchable swipe" uid="{{userid}}"\
	 action="/chat/nearbyinfo" action-type="next" >\
		<img src="/upload/{{userid}}.png"\
		{{#if rotate}} style="-webkit-transform:rotate({{rotate}}deg){{/if}}"\
		 height="60" width="60">\
		<div class="box">\
			<p>{{name}}</p>\
			<p>{{birthday}}</p>\
			<p>{{distance}}m</p>\
		</div>\
		<div class="sign">{{signature}}</div>\
	</div>{{/each}}',

	search: 
	'{{#each list}}{{#if name}}<div class="item touchable swipe" uid="{{_id}}" \
		action="/chat/searchinfo" action-type="next" >\
		<img src="/upload/{{_id}}.png" \
		{{#if rotate}} style="-webkit-transform:rotate({{rotate}}deg){{/if}}"\
		height="60" width="60">\
		<div class="box">\
			<p>{{name}}</p>\
			<p>{{birthday}}</p>\
		</div>\
		{{#if signature}}<div class="sign">{{signature}}</div>{{/if}}\
	</div>{{/if}}{{/each}}',

	feed: '\
	<div class="feed ">\
		<div class="header swipe touchable" uid="{{userid}}" action="/chat/feed" action-type="next">\
			<img src="/upload/{{userid}}.png" \
			{{#if rotate}} style="-webkit-transform:rotate({{rotate}}deg){{/if}}"\
			height="40" width="40">\
		</div>\
		<div class="cfeed">\
			<h3>{{name}}</h3>\
			<p>{{text}}</p>\
			<div class="imagelist">\
				{{#each imgs}}\
					<div><img src="/upload/{{this}}"></div>\
				{{/each}}\
			</div>\
		</div>\
	</div>',

	info: '\
		<div class="header">\
			<img src="/upload/{{_id}}.png"\
			{{#if rotate}} style="-webkit-transform:rotate({{rotate}}deg){{/if}}"\
			 height="52" width="52">\
			<span class="name">{{name}}</span>\
		</div>\
		<div class="items">\
			{{#if height}}\
			<div class="item">\
				<span class="title">身高</span>\
				<span class="c">{{height}}cm</span>\
			</div>\
			{{/if}}\
			<div class="item">\
				<span class="title">年龄</span>\
				<span class="c">{{age}}岁</span>\
			</div>\
			{{#if address}}\
			<div class="item">\
				<span class="title">所在地区</span>\
				<span class="c">{{address}}</span>\
			</div>\
			{{/if}}\
			<div class="item swipe touchable" style="height:70px" action="/chat/smyfeed" action-type="next"\
				uid={{_id}}>\
				<span class="title" style="vertical-align: top;">动态</span>\
				<span class="c" >\
				{{#each list}}<img src="/upload/{{this}}" width="60">{{/each}}\
				</span>\
			</div>\
		</div>\
		<div class="btn swipe touchable" action="/chat/message" action-type="next" mid="{{_id}}">聊天</div>\
		{{#if ismyfriend}}<div class="btn requestfriend">加好友</div>{{/if}}',

	notifications: '\
	{{#each list}}\
	<div class="item touchable" >\
		<img src="/upload/{{from}}.png"\
		{{#if rotate}} style="-webkit-transform:rotate({{rotate}}deg){{/if}}"\
		 height="40" width="40">\
		<div class="box">\
			<p>{{message.name}}</p>\
			<p style="color:#ccc">{{message.signature}}</p>\
		</div>\
		{{#if state}}\
			<div class="sign">{{#xxx state}}{{/xxx}}</div>\
		{{else}}\
			<div class="sign btn add" nid="{{_id}}" uid="{{from}}">添加</div>\
			<div class="sign btn refuse" nid="{{_id}}" uid="{{from}}">拒绝</div>\
		{{/if}}\
	</div>{{/each}}',

	friends: '\
	{{#each list}}\
	<div class="contact touchable swipe" action="/chat/info" uid="{{_id}}" action-type="next" >\
		<img src="/upload/{{_id}}.png"\
		{{#if rotate}} style="-webkit-transform:rotate({{rotate}}deg){{/if}}"\
		 height="30" width="30">\
		<span>{{name}}</span>\
	</div>{{/each}}'



};

Handlebars.registerHelper('xxx', function(state) {
	return state == "accept" ? '已接受' : '已拒绝';
});


for(var name in T){

	T[name] = Handlebars.compile(T[name]);
}


















