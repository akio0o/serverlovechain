

void function(){

	var host = location.host;

	if(location.host != "127.0.0.1" ) host = "106.185.41.253";

	window.socket = io.connect("http://"+host+":9000",{transports: ['websocket', 'polling', 'flashsocket']});
	window.Sync = {};

	var user = S.get("user");

	
	if(!user) return location.assign("/");

	Sync.contactlist = function(data){
		// var x = {}, lists = S.get("contactlist");
		// x.contacts = lists;
		// render(".chatlist", T.chatlist(x));
	};

	// socket.on("contactlist", function(data){
	// 	var x = {}, lists = [];

	// 	data.map(function(t){
	// 		if(t._id != user._id)
	// 			lists.push(t);
	// 	});
	// 	x.contacts = lists;
	// 	S.set("contactlist", lists);
	// 	render(".chatlist", T.chatlist(x));
	// });


	// socket.on("message", function(data){

	// 	append(".msgs", T.message(data));

	// 	updatecontact(data);

	// 	if(location.pathname == "/chat"){
	// 		render(".chatlist", T.chatlist({contacts: S.get("contactlist")}));
	// 	}
	// });

	// socket.on("renderhistory", function(messages){

	// 	var html = "",  lastto;

	// 	messages.map(function(message){
	// 		html += message.to == user._id ? T.message(message) : T.minemessage(message);
	// 	});
	// 	append(".msgs", html, true);
	// });



	socket.emit("connected");
	socket.emit("init", user);

	var body = $("body");

	// body.on("keydown", ".inputtext", function(e){
		
	// 	var text = this.value, current = S.get("messageid"), user = S.get("user");

	// 	if(e.keyCode == 13 && text.length > 0){

	// 		var data = {to: current, message: text,  from: user._id};
	// 		socket.emit("message", data);
	// 		append(".msgs", T.minemessage(data));
	// 		updateText(data);
	// 		this.value = "";
	// 	}
	// });


	function render(classname, html){
		$(classname).html(html);
	}

	function append(classname, html, clear){
		var dom = $(Swiper.activeSlide());
		var el = dom.find(classname);

		if(clear) el.html("");
		$(html).appendTo(el);

		el.scrollTop(999999);
	}


	function updatecontact(data){
		var contactlist = S.get("contactlist");
		var list = [], newmsg = [];

		contactlist.map(function(c){
			if(c._id == data.from){
				c.message = data.message;
				c.newmsg = c.newmsg+1;
				newmsg.push(c);
			}else{
				list.push(c);
			}
		});

		S.set("contactlist", newmsg.concat(list));
		socket.emit("savemsg", contactlist);
	}

	function updateText(data){
		var contactlist = S.get("contactlist");
		var list = [], newmsg = [];

		contactlist.map(function(c){
			if(c._id == data.to){
				c.message = data.message;
				newmsg.push(c);
			}else{
				list.push(c);
			}
		});

		S.set("contactlist", newmsg.concat(list));
		socket.emit("savemsg", contactlist);
	}

	function rendercontact(){

	}



	var MessageCenter = function(){
		this.list = {};
		this.messages = {};
		this.notifications = {};
		this.messagelist = [];
		this.friends = {};
	};


	MessageCenter.prototype = {


		init: function(){

			var me = this;

			socket.on("notifications", function(data){
				me.sync();
			});
			socket.on("messages", function(data){
				data.forEach(me.processmessage);
			});

			socket.on("message", me.processmessage);


			this.sync();
		},

		processmessage: function(data){

			// 如果在好友聊天页，则 添加信息
			data.id = data.from;
			MC.setMessage(data.from, data.message);

			if(location.pathname == "/chat/message" && S.get("messageid") == data.from){
				Sync.append(".messages", T.message(data));
			}

		},

		updatenumber: function(id, number, msg){
			var dom = $(Swiper.activeSlide()), el = dom.find("."+id); 
			el.find(".newmsg").html(number);
			el.find("p").html(msg);
			el.insertBefore( el.parent().find(".item").first() );
		},

		sync: function(){

			var me = this, user = S.get("user"), keys;

			$.get("/user/friends", function(data){
				me.list = data;

				// console.log(T.friends({list:data}))
				// keys = Object.keys(me.list);
				// socket.emit("messages", {from: user._id, keys: keys});
			});

			$.get("/user/notifications", function(data){
				me.notifications = data;
				me.process();
			});

			me.messagelist = S.get("messagelist") || [];

		},

		setMessage: function(id, message){
			var l = [], top, me = this;
			this.messagelist.map(function(x){
				if(x._id == id){
					x.message = message;
					top = x;
					if(location.pathname == "/chat"){
						x.newmsg = x.newmsg ? x.newmsg : 0;
						x.newmsg++;
					}
				}
				if(x._id != id) l.push(x);
			});
			if(!top){

				$.get("/user/userinfo/"+id, function(data){
					if(!data) return;
					top = data;
					top.message = message;
					top.newmsg = 1;
					this.messagelist = [top].concat(l);
					S.set("messagelist", this.messagelist);
					me.updatenumber(data._id, 1, message);
				});

			}else{
				this.messagelist = [top].concat(l);
				S.set("messagelist", this.messagelist);
				me.updatenumber(top._id, top.newmsg, message);
			}
		},

		clear: function(id){
			var me = this;
			this.messagelist.map(function(x){
				if(x._id == id) {
					x.newmsg = "";
					me.updatenumber(id, "", "");
				}
				return;
			});


		},


		process: function(){

			var fs = 0;

			this.notifications.map(function(n){
				if(n.isread == false){
					fs++;
				}
			});

			this.fs = fs;
			if(this.fs) $(".notifications").html(this.fs).show();


		},

		addmessage: function(user){

			var y = true;

			this.messagelist.forEach(function(x){
				if(x._id == user._id){
					y = false;
				}
			});

			if(y){
				this.messagelist = [user].concat(this.messagelist);
				S.set("messagelist", this.messagelist);
			}

		}



	};







	window.MC = new MessageCenter();


	MC.init();

	window.Sync = {};
	Sync.append = append;































}();