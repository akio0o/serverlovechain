


void function(){

	$("body").show();

	$(".swiper-slide,.wrapper").height(window.innerHeight/2);

	var arrow = $(".nav-arrow"), signin = $(".swiper-container");

	window.Swiper = new Swiper('.swiper-container',{
		// loop: true,
		speed: 300,

		// autoplayDisableOnInteraction:false,
		noSwiping:true,

		noSwipingClass: "touchable"
		// onTouchStart : function() {
		// }
	}); 
	var base64 = "";




UI.add("/", {
	template: "/templates/index/home.html",
	ishide: true,
	title: "链爱"
})

.add("/signin", {
	template: "/templates/index/signin.html",
	target: "/",
	title:"登录"
})

.add("/signup", {
	template: "/templates/index/signup.html",
	target: "/",
	title:  "注册"
})






// 注册 账号

.add("/verifyphone", {
	template: "/templates/index/verifyphone.html",
	target: "/signup",
	title: "验证手机号",
	callback: function(el, parent, callback){
		
		var number = parent.find(".phonenumber"),
			password = parent.find(".password");

		if( common.verify(common.isPhoneNumber, number) && common.verify(common.isPassword, password) ){

			$.post("/restful/User", {phoneNumber: number.val(), password: password.val()}, function(data){
				
				if(data.status == 1){
					alert(data.msg);
				}else{
					S.set("user", data);
					callback();
				}
			});
		}

		return false;
	},
	init: function(){

	}
})

.add("/chat", {
	template: "/templates/index/chat.html",
	target: "/signin",
	title: "聊天",
	callback: function(el, parent, callback){

		if(parent.find(".signin").html()){

			var number = parent.find(".phonenumber"), password = parent.find(".password");

			if( common.verify(common.isPhoneNumber, number) && common.verify(common.isPassword, password) ){
				$.post("/user/signin", {phoneNumber: number.val(), password: password.val()}, function(data){
					
					if(data.status == 1){
						alert(data.msg);
					}else{
						data.signin = true;
						S.set("user", data);
						location.assign("/chat");
					}
				});
			}

			return false;
		}



		var name = parent.find(".username"), birthday = parent.find(".birthday"),
			gender = parent.find(".gender .unchecked").siblings().attr("data"),
			state  = parent.find(".state .unchecked").siblings().attr("data"),
			data = {}, user = S.get("user");

		data.gender = gender;
		data.state  = state;
		data.name = name.val();
		data.birthday = birthday.val();

		if(!user._id) return false;

		if( common.verify(common.isName, name) && common.verify(common.isBirthday, birthday) ){
			$.post("/restful/User/"+user._id, data, function(data){
				
				if(data.status == 1) return ;

				data.signin = true;
				S.set("user", data);
				location = "/chat";
			});
		}
			
		return false;
	}
})

.add("/upload", {
	template: "/templates/index/upload.html",
	target: "/success",
	title: "上传照片",
	init: function(){

		var preview = document.querySelector('.upload img');
		var reader  = new FileReader();
		var c = document.getElementById("canvas"),
			wrap = $(".canvas");
		var width, height;
		var ctx = c.getContext("2d"), r = 0;

		function previewFile() {

			var file = document.querySelector('.upload input[type=file]').files[0];

			reader.onloadend = function () {
				base64 = reader.result;

				preview.onload = function(){

					width = 540;
					height = Math.ceil(preview.height*540/preview.width);

					console.log(width, height, preview.height, preview.width)

					ctx.drawImage(preview,0,0, width, height);
					c.parentNode.style.display = "block";
					console.log(preview.width);
				};
				preview.src = reader.result;
			}

			reader.readAsDataURL(file);
			
		}

		var point = {} , endpoint , x=0, y=0, a = 0, b =0;
		wrap.on("touchstart", function(e){
			if(e.touches.length == 1){
				point.x = e.touches[0].pageX;
				point.y = e.touches[0].pageY;
			}
			e.preventDefault();
		});

		wrap.on("touchmove", function(e){
			if(e.touches.length == 1){
				a = e.touches[0].pageX - point.x;
				b = e.touches[0].pageY - point.y;
				draw();
			}
			e.preventDefault();
		});

		wrap.on("touchend",function(e){
			x += a;
			y += b;
			a = 0;
			b = 0;
		});



		function draw(){
			ctx.drawImage(preview, a + x, b + y, width, height);

			ctx.restore();
		}

		function getImageData(){
			return c.toDataURL('image/jpeg');
		}


		$(".upload").find("input").on("change", previewFile);

		wrap.on("tap", ".upload", function(){
			var image = getImageData();
			$.post("/user/upload", { base64: image, rotate: r}, function(data){
				S.next("/basicinfo");
			});
		}).on("tap", ".reselect", function(){
			preview.onload = function(){};
			preview.src="/img/picture.png";
			wrap.hide();
		}).on("tap", ".rotate", function(){
			r += 90;
			$(c).css("transform","rotate("+r+"deg)");
		});
;
	}
})



.add("/success", {
	template: "/templates/index/success.html",
	target: "/verifyphone",
	title: "注册成功",
	callback: function(el, parent, callback){

		var code = parent.find(".code");

		if(common.verify(common.isVerifyCode, code)){

			$.get("/user/vcode/"+code.val(), function(data){
				if(data.ok){
					callback();
				}else{
					alert("验证码错误");
				}
			});
		}
	}
})

.add("/basicinfo", {
	template: "/templates/index/basicinfo.html",
	target: "/upload",
	title: "补充个人信息",
	callback: function(el, parent){

		

		return true;
	}
});


dev = false;

if(!dev){
	if(window.navigator.standalone) {
		// alert(S.getString("currentpage"))
		S.download();
		UI.render(S.getString("currentpage"));
	}else if(/Android/.test(navigator.userAgent)){
		S.download();
		UI.render(S.getString("currentpage"));
	}else{
		$(".loading").hide();
		$(".js-correct_browser").show();
	}
}else{
	S.download();

	// UI.render(S.getString("currentpage"));

}

$.ajaxSettings.beforeSend= function(xhr){
    xhr.setRequestHeader("X-Csrf-Token", $("#token").val());
};







}();



// Route.action("/", function(){
	// 	Swiper.swipePrev();
	// 	if(Swiper.activeIndex==1){
	// 		arrow.hide();
	// 	}
	// 	Route.nextPage("/signin");
	// });

	// Route.action("/signin",function(){
	// 	Swiper.swipeNext();
	// 	arrow.show();
	// 	Route.nextPage("/");
	// });

	
	
	// arrow.on("tap", function(){
	// 	Route.run("/");
	// });

	// signin.on("tap",function(){
	// 	Route.run("/signin");
	// });









