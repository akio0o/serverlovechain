
var server = require('http').createServer();
var io = require('socket.io').listen(server);

io.set('transports', ['websocket','flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling']);


// var contact = {};

// contact._id = "54844d2df15e250000612f10";
// contact.name = "LoveChain Team";
// contact.message = ": )";
// contact.newmsg = 0;


var sockets = {},
	messages = {},
	users = [];


var k = require("kmodel"), Messages = k.load("Messages");

var mq = {};


global.getSocket = function(id){
	return sockets[id] ? sockets[id] : {emit:function(){}};
};
io.on('connection', function(socket){


	socket.on("connected", function(){


		socket.on("init", function(data){

			socket.disconnected = false;
			sockets[data._id] = socket;
			mq[data._id] = mq[data._id] || [];
			socket.emit("messages", mq[data._id]);
			mq[data._id] = [];
		});

		socket.on("message", function(data){

			var socket = sockets[data.to];
			
			saveMessage(data);

			if(socket && socket.disconnected == false){
				socket.emit("message", data);
			}else{ 
				mq[data.to] = mq[data.to] || [];
				mq[data.to].push(data);
			}

		});

		socket.on("disconnect", function(){
			socket.disconnected = true;
		});
		
		
    });


});

server.listen(9000);



function saveMessage(data){

	data.key = data.to > data.from ? data.to+data.from : data.from + data.to;
	data.date = Date.now();
	data.ctime = new Date();


	var m = new Messages.model(data);

	m.save();
}



// function generatekey(key1, key2){

// 	var key = key1+key2;

// 	if(messages[key]) return key;

// 	return key2 + key1;
// }


function run (fn) {

    var gen = fn();

    next();

    function next(er, value){ 
        if (er) return gen.throw(er);
        var continuable = gen.next(value);

        if (continuable.done) return;
        var callback = continuable.value;
        callback(next);
    }
}
















